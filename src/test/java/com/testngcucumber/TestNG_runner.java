package com.testngcucumber;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/features/Testng.feature", glue="com.testngcucumber")

public class TestNG_runner extends AbstractTestNGCucumberTests {

}
/*import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
 
@CucumberOptions(tags = "", features = "src/test/resources/TestNg_Folder/TestNgfeature.feature", glue = "stepTestNg",plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)



public class TestRunnerTestNG extends AbstractTestNGCucumberTests{

}*/