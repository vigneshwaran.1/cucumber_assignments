package com.testngcucumber;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestNGsteps {
	static WebDriver driver = null;

	@Given("Open the browser")
	public void open_the_browser() {

		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	@Then("The home page of the website get displayed")
	public void the_home_page_of_the_website_get_displayed() {
		System.out.println("Homepage has Displayed");

	}

	@When("Click on Login button")
	public void click_on_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("Enter valid username{string} in the email textbox")
	public void enter_valid_username_in_the_email_textbox(String string) {
		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("Enter valid Password{string} in the password field")
	public void enter_valid_password_in_the_password_field(String string) {
		driver.findElement(By.id("Password")).sendKeys(string);
	}

	@Then("Click on the Login button")
	public void click_on_the_login_button() {
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();
	}

	@Then("the home page has displayed with logged user")
	public void the_home_page_has_displayed_with_logged_user() {
		System.out.println("Users Home page has displayed");
	}

	@Then("Click the Logout button")
	public void click_the_logout_button() throws InterruptedException {
		Thread.sleep(10000);
		driver.findElement(By.linkText("Log out")).click();
	}

	@Then("close the browser")
	public void close_the_browser() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}

}
