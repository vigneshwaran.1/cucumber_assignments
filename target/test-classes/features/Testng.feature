Feature: DemoWebShop

Scenario Outline: Verify Login funcionality for DemoWebShop

Given Open the browser

Then The home page of the website get displayed

When Click on Login button

And Enter valid username"<USERNAME>" in the email textbox

And Enter valid Password"<PASSWORD>" in the password field

Then Click on the Login button

And the home page has displayed with logged user

And Click the Logout button

And close the browser

Examples: 
      | USERNAME                | PASSWORD |
      | vignesgvgv@gmail.com    | Vicky@9080 |
      | vignesgvgv@gmail.com    | Vicky9080 |

   